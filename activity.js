db.singleRooms.insertOne({
	"name": "single",
	"accomodates": 2,
	"price": 1000,
	"description": "A simple room with all the basic necessities",
	"roomsAvailable": 10,
	"isAvailable": false
});

db.multipleRooms.insertMany([
	{
	"name": "double",
	"accomodates": 3,
	"price": 2000,
	"description": "A room fit for a small family going on vacation",
	"roomsAvailable": 5,
	"isAvailable": false
	},
	{
	"name": "queen",
	"accomodates": 4,
	"price": 4000,
	"description": "A room with a queen sized bed perfect for a simple getaway",
	"roomsAvailable": 15,
	"isAvailable": false
	}
]);

db.multipleRooms.find({
	"name": "double"
});

db.singleRooms.find({
	"name": "double"
});

db.multipleRooms.updateOne(
	{
		"name": "queen"
	},
	{
		$set: {
			"roomsAvailable": 0
		}
	}
);

db.multipleRooms.deleteMany({
	"roomsAvailable": 0
});

db.singleRooms.deleteMany({
	"roomsAvailable": 0
});